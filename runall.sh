#!/usr/bin/env bash

#get needed stuff 1st
./download.sh

#clean anything with same name to get rid of clashes
docker-compose down
docker-compose rm

#update with actual password
echo "password" > ./secrets/artifactoryPassword

#update older jenkins image, make sure it doesnt use cache
docker build --rm --build-arg HOST_DOCKER_GROUP_ID="`getent group docker | cut -d':' -f3`" -t myjenkins .

#run all
docker-compose -f docker-compose.yml -f docker-compose.override.yml up