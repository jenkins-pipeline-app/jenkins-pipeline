# Jenkins Setup

## Add Slack Integration

Create Credentials for slack token (should be done in groovy script): 

Use Jenkins Credentials and a credential ID to configure the Slack integration token. It is a security risk to expose your integration token using the previous *Integration Token* setting.

Create a new ***Secret text\*** credential:

[![image](image/slack-token.png)](image/slack-token.png)

Under Manage Jenkins -> Configure System, Scroll down to the "Slack" section:

Select that credential as the value for the ***Credential\*** field:

[![image](image/slack-plugin.png)](image/slack-plugin.png)

## Copy the SonarQube scanner binaries

1. download the sonarqubebinaries from https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.3.0.2102-linux.zip

2. extract the zip content uploaded into jenkins-misc/.sonar/

3. copy the binaries to $JENKINS_HOME/.sonar/native-sonar-scanner/sonar-scanner-4.3.0.2102-linux/bin



## Parameterized Jenkins Job

In Job General Setting, check "This project is parameterized", add a "String Parameter" with name and default value shown as below:

[![image-20200527160004511](image/parameterize-job.png)](image/parameterize-job.png)

Under Pipeline Section, change the "Branch to build" to variable ${BRANCH}

[![image-20200527160004511](image/parameterize-job-build.png)](image/parameterize-job-build.png)

Make sure "Lightweight checkout" is unchecked to evaluate the environment when pipeline start

## JFrog Deploy artifact by manual upload

```bash
2019-09-19T21:42:05.3625454Z [WARNING] The POM for commons-logging:commons-logging:jar:1.2 is invalid, transitive dependencies (if any) will not be available: 1 problem was encountered while building the effective model for commons-logging:commons-logging:1.2
2019-09-19T21:42:05.3626113Z [FATAL] 'parent.artifactId' must be changed, the parent element cannot have the same groupId:artifactId as the project. @ 

```

Question:
I have also encountered this problem (Artifactory version 6.11.3), and it is particularly problematic because the pom file that is created when there is a parent element uses the current artifact's coordinates as the parent coordinates, which causes a StackOverflow error from our build (gradle) as it tries to resolve dependencies using this pom file.  My "workaround" is to remove the pom file from the jar file I am deploying, in which case the generated pom file of course has no dependencies declared but at least my build will not completely fall over while building.  Of course, during run time there may be difficulties, but I think I have been lucky enough that any dependencies I removed from the actual pom file have been included elsewhere.

Answer:
there is no need to remove the pom from within the jar. Just upload the artifact in two steps:

First the jar alone, i.e. upload the jar and do not click on the option "Generate default POM/Deploy Jar's internal POM" This will upload just the jar.
Upload the pom. For that extract a copy of the internal jar and change the extension to ".pom" and upload it.

And that's it