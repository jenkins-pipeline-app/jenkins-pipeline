import jenkins.model.*
import org.jenkinsci.plugins.configfiles.maven.*
import org.jenkinsci.plugins.configfiles.maven.security.*

def configStore = Jenkins.instance.getExtensionList('org.jenkinsci.plugins.configfiles.GlobalConfigFiles')[0]

println("Setting maven settings xml")
def serverCreds = new ArrayList()


//server id as in your pom file
def serverId = 'asl-jfrog-artifactory'

//credentialId from credentials.groovy
def credentialId = 'jfrog'

serverCredentialMapping = new ServerCredentialMapping(serverId, credentialId)
serverCreds.add(serverCredentialMapping)


def configId =  'jenkins_jfrog_settings'
def configName = 'myMavenConfig for jenkins automation example'
def configComment = 'Global Maven Settings'
def configContent  = '''<settings>
<profiles>
   <profile>
     <id>jenkins-asl</id>
     <repositories>
       <repository>
         <id>cims-maven-repo</id>
         <name>cims-maven-repo</name>
         <url>http://artifactory:8081/artifactory/cims-maven-repo</url>
       </repository>
     </repositories>
   </profile>
 </profiles>
 <activeProfiles>
   <activeProfile>jenkins-asl</activeProfile>
 </activeProfiles>
</settings>'''


def globalConfig = new GlobalMavenSettingsConfig(configId, configName, configComment, configContent, true, serverCreds)
configStore.save(globalConfig)

println("maven settings complete")
