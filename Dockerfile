FROM jenkins/jenkins:2.236

ARG HOST_DOCKER_GROUP_ID
RUN id

USER root
#RUN chown -R jenkins:jenkins "$JENKINS_HOME"/
#RUN chmod -R 777 /usr/share/jenkins/ref/trigger-job.sh

# Create 'docker' group with provided group ID 
# and add 'jenkins' user to it
RUN groupadd docker -g ${HOST_DOCKER_GROUP_ID} && \
    usermod -a -G docker jenkins


RUN apt-get update && apt-get install -y tree nano curl sudo
RUN curl https://get.docker.com/builds/Linux/x86_64/docker-latest.tgz | tar xvz -C /tmp/ && mv /tmp/docker/docker /usr/bin/docker
RUN curl -L "https://github.com/docker/compose/releases/download/1.25.5/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
RUN chmod 755 /usr/local/bin/docker-compose
RUN usermod -a -G sudo jenkins

RUN echo "jenkins ALL=(ALL:ALL) NOPASSWD:ALL" >> /etc/sudoers

RUN newgrp docker

USER jenkins
COPY plugins.txt /usr/share/jenkins/ref/plugins.txt
RUN /usr/local/bin/install-plugins.sh < /usr/share/jenkins/ref/plugins.txt
 
# Setting up environment variables for Jenkins admin user
ENV JENKINS_USER admin
ENV JENKINS_PASS admin
 
# Skip the initial setup wizard
ENV JAVA_OPTS -Djenkins.install.runSetupWizard=false

COPY jenkins.properties ${JENKINS_HOME}/
# Start-up scripts to set number of executors and creating the admin user
COPY groovy/* /usr/share/jenkins/ref/init.groovy.d/
 
# Name the jobs  
# the job config will be mounted during docker run

#COPY credentials/credentials.xml /usr/share/jenkins/ref/
COPY scripts/trigger-job.sh /usr/share/jenkins/ref/
USER root
RUN chown -R 777 /usr/share/jenkins/ref/trigger-job.sh
USER jenkins
# Add the custom configs to the container  
#COPY ${job_name_1}_config.xml "$JENKINS_HOME"/jobs/${job_name_1}/config.xml  

#ENTRYPOINT ["/bin/sh -c /var/jenkins_home/trigger-job.sh"]